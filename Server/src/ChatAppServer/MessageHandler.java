package ChatAppServer;

import ChatAppServer.ControlFunction.SocketHandler;
import ChatAppServer.Models.MessageInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static ChatAppServer.RoomHandler.createRoomPath;

public final class MessageHandler {
    private MessageHandler(){}

    /**
     * Adds message from client to room json
     * @param socket
     * @throws IOException
     */
    public static void processMessage(Socket socket) throws IOException {
        var gson = new Gson();
        var inOuts = new SocketHandler(socket);
        MessageInfo lines = gson.fromJson(inOuts.getInput().readLine(), new TypeToken<MessageInfo>() {
        }.getType());
        List<MessageInfo> messages = gson.fromJson(new String(Files.readAllBytes(Paths.get(createRoomPath(lines.getRoomName())))), new TypeToken<ArrayList<MessageInfo>>() {
        }.getType());
        messages.add(lines);
        var myWriter = new FileWriter(createRoomPath(lines.getRoomName()));
        myWriter.write(gson.toJson(messages));
        myWriter.close();
        socket.close();

    }

    /**
     * Initial load of messages of given room
     * @param socket
     * @throws IOException
     */
    public static void initialLoad(Socket socket) throws IOException {
        var inOuts = new SocketHandler(socket);
        var mess = new String(Files.readAllBytes(Paths.get(createRoomPath(inOuts.getInput().readLine()))));
        inOuts.getOutput().println(mess);
        inOuts.getOutput().flush();
        socket.close();
    }



}
