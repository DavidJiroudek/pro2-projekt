package ChatAppServer.ControlFunction;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketHandler {
    private PrintWriter output;
    private BufferedReader input;
    private Socket socket;

    private static final int PORT = 5000;
    private static final String LOCALHOST = "127.0.0.1";

    public SocketHandler(Socket socket){
        try {
            output = new PrintWriter(socket.getOutputStream());
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.socket = socket;
        }catch (Exception ex){
            System.out.println("Connection cannot be established");
        }
    }
    public SocketHandler(){
        try {
            var socket = new Socket(LOCALHOST, PORT);
            output = new PrintWriter(socket.getOutputStream());
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.socket = socket;
        }catch (Exception ex){
            System.out.println("Connection cannot be established(SocketHandler)");
        }
    }

    public PrintWriter getOutput() {
        return output;
    }

    public void setOutput(PrintWriter output) {
        this.output = output;
    }

    public BufferedReader getInput() {
        return input;
    }

    public void setInput(BufferedReader input) {
        this.input = input;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}
