package ChatAppServer;

import ChatAppServer.ControlFunction.SocketHandler;
import ChatAppServer.Models.MessageInfo;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.util.Collections;
import java.util.List;

public final class RoomHandler {
    private RoomHandler() {
    }


    /**
     * Gets list of available rooms
     *
     * @param socket
     * @throws IOException
     */
    public static void getRooms(Socket socket) throws IOException {
        var files = new File("Rooms");
        var pathNames = files.list();
        var inOuts = new SocketHandler(socket);
        if (pathNames != null) {
            inOuts.getOutput().println(String.join(";", pathNames));
        }
        inOuts.getOutput().flush();
        socket.close();
    }

    /**
     * Creates new room with given name if not yet exists
     *
     * @param socket
     * @throws IOException
     */
    public static void createRoom(Socket socket) throws IOException {
        var gson = new Gson();
        var inOuts = new SocketHandler(socket);
        var roomName = inOuts.getInput().readLine();
        var path = createRoomPath(roomName);
        File textFile = new File(path);
        if (textFile.createNewFile()) {
            System.out.println("File created: " + textFile.getName());
            var myWriter = new FileWriter(path);
            List<MessageInfo> initMess = Collections.singletonList(new MessageInfo("SERVER", roomName, "Vytvořena nová chatovací místnost"));
            myWriter.write(gson.toJson(initMess));
            myWriter.close();
        } else {
            System.out.println("File already exists.");
            inOuts.getOutput().println("File already exists.");
            inOuts.getOutput().flush();
        }
        socket.close();
    }


    public static String createRoomPath(String name) {
        return ("Rooms/" + name + ".txt");
    }
}
