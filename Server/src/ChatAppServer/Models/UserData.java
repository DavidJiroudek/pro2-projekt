package ChatAppServer.Models;

public class UserData {
    private String userName;
    private String selectedRoom;

    public UserData(String userName, String selectedRoom) {
        this.userName = userName;
        this.selectedRoom = selectedRoom;
    }
    public UserData(){
    }

    //region GetSets
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSelectedRoom() {
        return selectedRoom;
    }

    public void setSelectedRoom(String selectedRoom) {
        this.selectedRoom = selectedRoom;
    }
    //endregion
}
