package ChatAppServer.Models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageInfo {
    private String username;
    private Date date;
    private String roomName;
    private String message;


    public MessageInfo(String username, String roomName, String message) {
        this.username = username;
        this.roomName = roomName;
        this.date = new Date();
        this.message = message;
    }

    public MessageInfo() {
    }

    @Override
    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        return (dateFormat.format(date) + " " + username + ": \n" + message + "\n\n");
    }

    //region GetSets
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
    //endregion
}
