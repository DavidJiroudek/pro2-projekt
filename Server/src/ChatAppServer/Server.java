package ChatAppServer;

import java.net.ServerSocket;
import java.net.Socket;

import static ChatAppServer.LoginHandler.addUser;
import static ChatAppServer.LoginHandler.loginUser;
import static ChatAppServer.MessageHandler.initialLoad;
import static ChatAppServer.MessageHandler.processMessage;
import static ChatAppServer.RoomHandler.createRoom;
import static ChatAppServer.RoomHandler.getRooms;

public class Server {
    private static final int PORT = 5000;
    private ServerSocket serverSocket;

    public Server() {
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (Exception ex) {
            System.out.println("Connection cannot be established (server)");
        }
    }

    private void run() {
        System.out.println("Server is running");
        while (true) {
            try {
                Socket s = serverSocket.accept();
                switch ((char) s.getInputStream().read()) {
                    case 'i' -> initialLoad(s);
                    case 's' -> processMessage(s);
                    case 'r' -> getRooms(s);
                    case 'c' -> createRoom(s);
                    case 'l' -> loginUser(s);
                    case 'a' -> addUser(s);
                    default -> {
                    }
                }
            } catch (Exception ex) {
                System.out.println("Connection cannot be established(Socket)");
            }
        }
    }

    public static void main(String[] args) {
        try {
            new Server().run();
        } catch (Exception exception) {
            System.out.println("Server cannot be started" + exception);
        }
    }
}
