package ChatAppServer;

import ChatAppServer.ControlFunction.SocketHandler;
import ChatAppServer.Models.UserLogin;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileWriter;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public final class LoginHandler {
    private LoginHandler() {
    }
    private static final String USERS = "DefinitelyNotPasswords/WhiteFluffyUnicorn.txt";

    /**
     * Sends passHash and salt to client
     * @param socket
     * @throws IOException
     */
    public static void loginUser(Socket socket) throws IOException {
        var gson = new Gson();
        var inOuts = new SocketHandler(socket);

        List<UserLogin> users = gson.fromJson(new String(Files.readAllBytes(Paths.get(USERS))), new TypeToken<ArrayList<UserLogin>>() {
        }.getType());
        var password = inOuts.getInput().readLine();
        for (var user : users) {
            if (password.equals(user.getUsername())) {
                inOuts.getOutput().println(gson.toJson(user));
                inOuts.getOutput().flush();
                return;
            }
        }
        inOuts.getOutput().println("false");
        inOuts.getOutput().flush();
        socket.close();
    }

    /**
     * Adds new user to the file with credentials
     * @param socket
     * @throws IOException
     */
    public static void addUser(Socket socket) throws IOException {
        var gson = new Gson();
        var inOuts = new SocketHandler(socket);

        UserLogin inputData = gson.fromJson(inOuts.getInput().readLine(), new TypeToken<UserLogin>() {}.getType());
        List<UserLogin> users = gson.fromJson(new String(Files.readAllBytes(Paths.get(USERS))), new TypeToken<ArrayList<UserLogin>>() {}.getType());
        if (users != null) {
            for (var user : users) {
                if (inputData.getUsername().equals(user.getUsername())) {
                    inOuts.getOutput().println("false");
                    inOuts.getOutput().flush();
                    socket.close();
                    return;
                }
            }
        } else {
            users = new ArrayList<>() {};
        }
        users.add(inputData);

        var myWriter = new FileWriter(USERS);
        var text = gson.toJson(users, new TypeToken<ArrayList<UserLogin>>() {}.getType());
        myWriter.write(text);
        myWriter.close();

        inOuts.getOutput().println("true");
        inOuts.getOutput().flush();
        socket.close();
    }
}
