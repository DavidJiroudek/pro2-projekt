package ChatApp;


import ChatApp.ControlFunction.SocketHandler;
import ChatApp.ControlFunction.WindowFunctions;
import ChatApp.Models.UserData;
import ChatApp.Models.UserLogin;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

import static ChatApp.ControlFunction.HashHandler.hashPassword;

public class LoginController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TextField txtUserName;
    @FXML
    private TextField txtPassword;
    @FXML
    private Label lblWrongPass;
    @FXML
    private Label lblExistingAccount;

    private Gson gson;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        lblExistingAccount.setVisible(false);
        lblWrongPass.setVisible(false);
        gson = new Gson();
    }

    /**
     * Takes data from login fields then gets hash of given name from server and compares
     */
    public void loginUser() {
        var inOuts = new SocketHandler();
        try {
        inOuts.getOutput().println("l" + txtUserName.getText());
        inOuts.getOutput().flush();
            UserLogin user = gson.fromJson(inOuts.getInput().readLine(), new TypeToken<UserLogin>() {}.getType());
            var hashResult = hashPassword(txtUserName.getText(), user.getSalt());
            if (Arrays.equals(hashResult.getKey(), user.getPassword())) {
                Stage stage = (Stage) anchorPane.getScene().getWindow();
                stage.close();
                WindowFunctions.openWindow(new FXMLLoader(getClass().getResource("FXML/Rooms.fxml")), new UserData(txtUserName.getText(), null));
            }
            inOuts.getSocket().close();
        } catch (Exception ex) {
            System.out.println("Connection cannot be established(LoginUser)" + ex);
            lblWrongPass.setVisible(true);
        }
    }

    /**
     * Takes data from login fields then sends them to server to validate
     */
    public void addUser() {
        var inOuts = new SocketHandler();
        try {
            var hash = hashPassword(txtPassword.getText());

            inOuts.getOutput().println("a" + gson.toJson(new UserLogin(txtUserName.getText(), hash.getKey(), hash.getValue())));
            inOuts.getOutput().flush();

            if (inOuts.getInput().readLine().equals("true")) {
                Stage stage = (Stage) anchorPane.getScene().getWindow();
                stage.close();
                WindowFunctions.openWindow(new FXMLLoader(getClass().getResource("FXML/Rooms.fxml")), new UserData(txtUserName.getText(), null));
            }
            inOuts.getSocket().close();
        } catch (Exception ex) {
            System.out.println("Connection cannot be established(AddUser)" + ex);
        }
        lblExistingAccount.setVisible(true);
    }

    @FXML
    private void login(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            loginUser();
        }
    }


}

