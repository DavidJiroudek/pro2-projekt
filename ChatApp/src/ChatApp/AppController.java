package ChatApp;

import ChatApp.ControlFunction.SocketHandler;
import ChatApp.ControlFunction.WindowFunctions;
import ChatApp.Models.MessageInfo;
import ChatApp.Models.UserData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.net.URL;
import java.util.*;

public class AppController implements Initializable {
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TextArea messageField;
    @FXML
    private TextArea sendField;
    @FXML
    private Button btnLogOff;
    @FXML
    private Button btnSend;


    private static final int PORT = 5000;
    private static final String LOCALHOST = "127.0.0.1";


    private Stage currentScene;
    private UserData userData;
    private String roomName;
    private Gson gson;
    private List<MessageInfo> messages;
    private String plainText = "";
    private Timer timer;


    //initialization of FXML
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        timer = new Timer();
        messages = getMessages();
        for (var item : messages) {
            plainText +=  item.toString();
        }
        messageField.setText(plainText);
        messageField.selectEnd();
        messageField.deselect();

        try{
            timer();

        }catch (Exception ex){
            System.out.println("Timer not set");
        }
    }

    //Constructors
    public AppController(String roomName) {
        gson = new Gson();
        this.roomName = roomName;

    }

    public AppController(List<MessageInfo> messages) {

    }
private void showMessages(){


}
    /**
     * Takes massage from field and sends it to the server
     */
    @FXML
    private void sendMessage() {
        currentScene = (Stage) anchorPane.getScene().getWindow();
        userData = (UserData) currentScene.getUserData();

        if (sendField.getText().equals("")) {
            return;
        }
        var message = new MessageInfo(userData.getUserName(), userData.getSelectedRoom(), sendField.getText());
        var messageJson = gson.toJson(message);
        var inOuts = new SocketHandler();
        try {
            inOuts.getOutput().println("s" + messageJson);
            inOuts.getOutput().flush();
            inOuts.getSocket().close();
        } catch (Exception ex) {
            System.out.println("Connection cannot be established (sendMassage)");
        }
        messages.add(message);
        messageField.setText(plainText = plainText + message);
        sendField.setText("");
        messageField.selectEnd();
        messageField.deselect();
    }

    /**
     * Initial load of messages for selected room
     */
    @FXML
    private List<MessageInfo> getMessages() {
        var inOuts = new SocketHandler();
        try {
            inOuts.getOutput().println("i" + roomName);
            inOuts.getOutput().flush();
            var result = inOuts.getInput().readLine();
            System.out.println(result);
            return (gson.fromJson(result, new TypeToken<ArrayList<MessageInfo>>() {}.getType()));
        } catch (Exception ex) {
            System.out.println("Connection cannot be established (getMessages)");
        }
        return (null);
    }

    //log off function
    @FXML
    private void logOff() {
        currentScene = (Stage) anchorPane.getScene().getWindow();
        userData = (UserData) currentScene.getUserData();
        currentScene.close();
        timer.cancel();
        timer.purge();
        WindowFunctions.openWindow(new FXMLLoader(getClass().getResource("FXML/Rooms.fxml")), userData);

    }
    public void timer() throws InterruptedException
    {

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                plainText = "";
                var mess= getMessages();
                for (var item : mess) {
                    plainText +=  item.toString();
                }
                messageField.setText(plainText);
                System.out.println("roflmao");
                messageField.setScrollTop(Double.MAX_VALUE);
            }
        }, 3000, 3000);
    }

}
