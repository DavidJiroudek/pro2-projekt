package ChatApp;

import ChatApp.ControlFunction.SocketHandler;
import ChatApp.ControlFunction.WindowFunctions;
import ChatApp.Models.UserData;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class RoomsController implements Initializable {
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private ListView<String> roomsView;
    @FXML
    private TextField newRoomName;
    @FXML
    private Label lblRoomExists;

    private Stage currentScene;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadRooms();
        roomsView.getSelectionModel().select(0);
        lblRoomExists.setVisible(false);
    }

    /**
     * Tries to load list of rooms from the server
     */
    private void loadRooms() {
        try {
            var inOuts = new SocketHandler();

            inOuts.getOutput().println("r");
            inOuts.getOutput().flush();

            var lines = inOuts.getInput().readLine().split(";");
            var cutLines = new ArrayList<String>();
            for (var item : lines) {
                cutLines.add(item.substring(0, item.length() - 4));
            }

            roomsView.getItems().clear();
            roomsView.setItems(FXCollections.observableList(cutLines));
            inOuts.getSocket().close();
        } catch (Exception ex) {
            System.out.println("Connection cannot be established(loadRooms)");
        }
    }

    /**
     * Opens new chat window with messages from selected room
     */
    @FXML
    private void enterRoom() {
        enterRoom(roomsView.getSelectionModel().getSelectedItem());
    }

    /**
     * Opens new chat window with messages from selected room(Name based)
     *
     * @param roomName Name of the room
     */
    private void enterRoom(String roomName) {
        if (!roomsView.getSelectionModel().isEmpty()){
            currentScene = (Stage) anchorPane.getScene().getWindow();
            currentScene.close();
            var userData = (UserData) currentScene.getUserData();
            WindowFunctions.openWindow(new FXMLLoader(getClass().getResource("FXML/App.fxml")), new UserData(userData.getUserName(), roomName), new AppController(roomName));
        }
    }

    /**
     * Creates a room. Name taken from given textField
     */
    @FXML
    private void createRoom() {
        try {
            var inOuts = new SocketHandler();
            inOuts.getOutput().println("c" + newRoomName.getText());
            inOuts.getOutput().flush();
            var response = inOuts.getInput().readLine();
            if (response != null) {
                lblRoomExists.setVisible(true);
            } else {
                enterRoom(newRoomName.getText());
            }
            inOuts.getSocket().close();
        } catch (Exception ex) {
            System.out.println("Connection cannot be established");
        }
    }

    @FXML
    private void logOff() {
        currentScene = (Stage) anchorPane.getScene().getWindow();
        currentScene.close();
        WindowFunctions.openWindow(new FXMLLoader(getClass().getResource("FXML/Login.fxml")), null);

    }
}
