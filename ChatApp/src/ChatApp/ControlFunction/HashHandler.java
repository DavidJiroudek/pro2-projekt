package ChatApp.ControlFunction;

import javafx.util.Pair;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

public final class HashHandler {

    private HashHandler(){}

    public static Pair<byte[], byte[]> hashPassword(String password) {
        SecureRandom random = new SecureRandom();
        var salt = new byte[16];
        random.nextBytes(salt);
        return hashPassword(password, salt);

    }

    //returns pair of hash and salt
    public static Pair<byte[], byte[]> hashPassword(String password, byte[] salt) {
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65534, 128);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

            var hash = factory.generateSecret(spec).getEncoded();
            System.out.println(hash + "   " + salt);
            return new Pair<byte[], byte[]>(hash, salt);

        } catch (Exception ex) {
            System.out.println("Hash cannot be created" + ex);
            return null;
        }
    }
}
