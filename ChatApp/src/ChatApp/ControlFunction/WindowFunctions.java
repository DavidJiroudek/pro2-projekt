package ChatApp.ControlFunction;

import ChatApp.AppController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class WindowFunctions {

    public static void openWindow(FXMLLoader fxmlLoader, Object userName) {
        try {

            Parent root = (Parent) fxmlLoader.load();
            Stage newStage = new Stage();
            newStage.setTitle("ChatApp");
            newStage.setScene(new Scene(root));
            newStage.setUserData(userName);
            newStage.show();
        } catch (Exception ex) {
            System.out.println("Chyba při vytvorení okna" + ex );
        }
    }
    //needed when userdata operates in initialization -> sends them thrue constructor
    public static void openWindow(FXMLLoader fxmlLoader, Object userName, Object controller) {
        try {
            fxmlLoader.setController(controller);
            Parent root = (Parent) fxmlLoader.load();
            Stage newStage = new Stage();
            newStage.setTitle("ChatApp");
            newStage.setScene(new Scene(root));
            newStage.setUserData(userName);
            newStage.show();
        } catch (Exception ex) {
            System.out.println("Chyba při vytvorení okna" + ex );
        }
    }

}
